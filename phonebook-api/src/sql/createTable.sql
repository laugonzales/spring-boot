-- user=root, password=root
Create database phonebook;
CREATE TABLE contact
( id INT(11) NOT NULL AUTO_INCREMENT,
  name VARCHAR(50) NOT NULL,
  description  VARCHAR(100),
  phone  VARCHAR(15),
  CONSTRAINT person_pk PRIMARY KEY (id)
);