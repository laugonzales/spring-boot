package com.laurence.controllers;

import com.laurence.models.Contact;
import com.laurence.models.ContactDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;

@RestController
@RequestMapping("/phonebook")
public class PhoneBookRestController {

    @Autowired
    private ContactDao contactDao;

    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity<Collection<Contact>> getAllContacts() {
        return new ResponseEntity<>((Collection<Contact>) contactDao.findAll(), HttpStatus.OK);
    }

    @RequestMapping(path="/contact/{id}",method = RequestMethod.GET)
    public ResponseEntity<Contact> getContactWithId(@PathVariable Long id) {
        return new ResponseEntity<>(contactDao.findOne(id), HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.GET, params = {"name"})
    public ResponseEntity<Collection<Contact>> findContactWithName(@RequestParam(value = "name") String name) {
        return new ResponseEntity<>(contactDao.findByName(name), HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.PUT)
    public ResponseEntity<?> addContact(@RequestBody Contact input) {
        return new ResponseEntity<>(contactDao.save(input), HttpStatus.CREATED);
    }

    @RequestMapping(path="/contact/{id}",method = RequestMethod.DELETE)
    public ResponseEntity<?> deleteContact(@PathVariable Long id) {
        try {
            contactDao.delete(id);
            return ResponseEntity.noContent().build();
        } catch (Exception e) {
            return ResponseEntity.notFound().build();
        }
    }
}
