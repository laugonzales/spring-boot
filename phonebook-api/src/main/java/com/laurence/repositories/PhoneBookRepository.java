package com.laurence.repositories;

import com.laurence.models.Contact;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface PhoneBookRepository extends CrudRepository<Contact, Long> {
    List<Contact> findByName(String name);
}
