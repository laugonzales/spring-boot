package com.laurence.models;

import com.laurence.models.Contact;
import org.springframework.data.repository.CrudRepository;

import javax.transaction.Transactional;
import java.util.Collection;

@Transactional
public interface ContactDao extends CrudRepository<Contact, Long> {
    Collection<Contact> findByName(String name);
}
