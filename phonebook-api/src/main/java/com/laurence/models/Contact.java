package com.laurence.models;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

/**
 * An entity Contact composed by three fields (id, phone, name).
 */
@Entity
@Table(name = "contacts")
public class Contact {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @NotNull
    private String name;

    @NotNull
    private String phone;

    private String description;

    public Contact() {}

    public Contact(String name, String phone, String description) {
        this.name = name;
        this.phone = phone;
        this.description = description;
    }

    public Long getId() {return id;}

    public String getName() {
        return name;
    }

    public String getPhone() {
        return phone;
    }

    public String getDescription() {
        return description;
    }

}
